package com.example.future_mind.base.presentation

import android.app.Application
import com.example.future_mind.base.Architecture
import com.example.future_mind.base.di.databaseModule
import com.example.future_mind.base.di.networkModule
import com.example.future_mind.information.di.futureMindModule
import io.realm.Realm
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.logger.EmptyLogger

class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {

            androidContext(this@BaseApplication)
            modules(
                listOf(
                    databaseModule,
                    networkModule,
                    futureMindModule
                )
            )
            logger(EmptyLogger())
        }

        Realm.init(this)
        val repository: Architecture.LocalRepository by inject()
        repository.init()
    }
}
