package com.example.future_mind.base.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.example.future_mind.R
import com.example.future_mind.base.Architecture


abstract class BaseFragment<T : BaseViewModel>(
    private val contentLayoutId: Int
) : Fragment(contentLayoutId) {
    protected abstract val viewModel: T
    private val navController by lazy {
        requireActivity().findNavController(R.id.nav_host_fragment_container)
    }
    private val navDetailsController by lazy {
        requireActivity().findNavController(R.id.nav_host_fragment_details_container)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupViewModel()
    }

    private fun setupViewModel() {
        this.viewModel.prepare(arguments)

        this.viewModel.navigationCommands.observe(viewLifecycleOwner, Observer { command ->
            resolveNavigationCommand(command)
        })
    }

    private fun resolveNavigationCommand(command: Architecture.NavigationCommand) {
        when (command) {
            is Architecture.NavigationCommand.To -> {
                navController.navigate(command.directions)
            }
            is Architecture.NavigationCommand.ToOtherNavigation -> {
                navDetailsController.navigate(command.directions)
            }
        }
    }
}