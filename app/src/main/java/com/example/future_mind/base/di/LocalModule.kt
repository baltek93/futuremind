package com.example.future_mind.base.di

import com.example.future_mind.base.Architecture
import com.example.future_mind.base.data.local.PredicateFactory
import com.example.future_mind.base.data.local.RealmLocalRepository
import com.example.future_mind.base.data.local.RealmPredicateFactory
import io.realm.Realm
import io.realm.RealmConfiguration
import org.koin.dsl.module

val databaseModule = module{

    factory<RealmConfiguration> {
        RealmConfiguration.Builder()
            .deleteRealmIfMigrationNeeded()
            .modules(
                Realm.getDefaultModule()
            )
            .build()
    }

    single<Architecture.LocalRepository> {
        RealmLocalRepository(get())
    }

    single<PredicateFactory> {
        RealmPredicateFactory()
    }
}