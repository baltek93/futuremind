package com.example.future_mind.base

import androidx.navigation.NavDirections
import com.example.future_mind.base.data.local.Predicate
import io.reactivex.Observable
import java.util.*

interface Architecture {
    interface LocalRepository {

        fun <T> saveData(list: List<T>)
        fun <T> saveData(item: T)

        fun <T> getListObservable(t: Class<T>,predicate: Predicate?=null): Observable<List<T>>
        fun <T> getObservable(t: Class<T>): Observable<T>
        fun <T, T2> getObservable(
            t: Class<T>,
            key: String? = null,
            value: T2? = null
        ): Observable<T>

        fun init()
        fun deInit()
    }

    interface RestConfiguration {
        val url: String
    }

    interface DateFormatter {
        fun parseDate(dateString: String): Date?
        fun serializeDate(date: Date?): String
        fun formatDate(date: Date?): String
        fun parseAndFormatDate(dateString: String): String
        fun parse(dateString: String, formatString: String): Date?
    }

    interface SeparatorDescriptionAndUrl{
        fun getOnlyDescription(descriptionWithUrl: String): String
        fun getOnlyUrl(descriptionWithUrl: String): String

    }
    sealed class NavigationCommand {
        data class To(val directions: NavDirections) : NavigationCommand()
        data class ToOtherNavigation(val directions: NavDirections) : NavigationCommand()
    }


    enum class UiState {
        LOADING, SUCCESS, FAILED
    }

    companion object{
        const val GLIDE_OK_HTTP="GLIDE_OK_HTTP"
    }
}