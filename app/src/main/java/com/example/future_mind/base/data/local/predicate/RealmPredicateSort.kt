package com.example.future_mind.base.data.local.predicate

import com.example.future_mind.base.data.local.Order
import com.example.future_mind.base.data.local.RealmPredicate
import io.realm.RealmQuery
import io.realm.Sort

class RealmPredicateSort(
    private val predicate: RealmPredicate?, private val nameField: String,
    private val sortOrder: Order
) : RealmPredicate {
    override fun apply(query: RealmQuery<*>): RealmQuery<*> {
        if (predicate != null) {
            if (sortOrder == Order.ASCENDING) {
                return predicate.apply(query).sort(nameField, Sort.ASCENDING)

            } else if (sortOrder == Order.DESCENDING) {
                return predicate.apply(query).sort(nameField, Sort.DESCENDING)
            }
        } else {
            if (sortOrder == Order.ASCENDING) {
                return query.sort(nameField, Sort.ASCENDING)

            } else if (sortOrder == Order.DESCENDING) {
                return query.sort(nameField, Sort.DESCENDING)
            }
        }

        return query
    }

}