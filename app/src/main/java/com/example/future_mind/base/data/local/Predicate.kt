package com.example.future_mind.base.data.local

import io.realm.RealmQuery

interface Predicate

interface RealmPredicate : Predicate {
    fun apply(query: RealmQuery<*>): RealmQuery<*>
}

enum class Order {
    ASCENDING, DESCENDING
}

