package com.example.future_mind.base.utils

import com.example.future_mind.base.Architecture

class SeparatorDescriptionAndUrlImpl : Architecture.SeparatorDescriptionAndUrl {

    override fun getOnlyDescription(descriptionWithUrl: String): String {
        val listStrings = descriptionWithUrl.split("\t")

        if (listStrings.isNotEmpty()) {
            return listStrings[0]
        } else {
            return ""
        }
    }

    override fun getOnlyUrl(descriptionWithUrl: String): String {
        val listStrings = descriptionWithUrl.split("\t")

        if (listStrings.isNotEmpty()) {
            return listStrings.last()
        } else {
            return ""
        }
    }


}