package com.example.future_mind.base.data.local

import com.example.future_mind.base.data.local.predicate.RealmPredicateSort

interface PredicateFactory {
    fun sort(predicate: Predicate?, nameField: String, sortOrder: Order): Predicate
}

class RealmPredicateFactory : PredicateFactory {
    override fun sort(predicate: Predicate?, nameField: String, sortOrder: Order): Predicate {
        require(predicate is RealmPredicate?)
        return RealmPredicateSort(predicate, nameField, sortOrder)
    }
}