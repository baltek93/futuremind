package com.example.future_mind.base.utils

import com.example.future_mind.base.Architecture
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

/*
 parse, which means parsing json response to Date object
 format, which formats Date object to app format
 serialize, which serializes Date object to API format
 */
class DateFormatterImpl(
    val apiDateFormat: String = API_DATE_FORMAT,
    val appDateFormat: String = APP_DATE_FORMAT
) : Architecture.DateFormatter {

    override fun parseDate(dateString: String): Date? {
        return parse(dateString, apiDateFormat)
    }

    override fun serializeDate(date: Date?): String {
        return serialize(date, apiDateFormat)
    }


    override fun formatDate(date: Date?): String {
        return format(date, appDateFormat)
    }

    override fun parseAndFormatDate(dateString: String): String {
        return format(parseDate(dateString), appDateFormat)
    }

    override fun parse(dateString: String, formatString: String): Date? {
        return try {
            val dateFormat = SimpleDateFormat(formatString, Locale.getDefault())
            dateFormat.parse(dateString)
        } catch (ex: Exception) {
            null
        }
    }

    private fun format(date: Date?, formatString: String): String {
        val dateFormat = SimpleDateFormat(formatString, Locale.getDefault())

        return if (date != null) {
            dateFormat.format(date)
        } else {
            ""
        }
    }

    private fun serialize(date: Date?, formatString: String): String {
        return format(date, formatString)
    }


    companion object {
        const val API_DATE_FORMAT = "yyyy-MM-dd"
        const val APP_DATE_FORMAT = "dd/MM/yyyy"
    }
}