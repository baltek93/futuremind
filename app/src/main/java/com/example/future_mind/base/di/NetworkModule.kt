package com.example.future_mind.base.di

import com.example.future_mind.base.Architecture
import com.example.future_mind.base.Architecture.Companion.GLIDE_OK_HTTP
import com.example.future_mind.base.data.local.RealmListJsonAdapterFactory
import com.example.future_mind.information.data.remote.FutureMindRestConfiguration
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

val networkModule = module {
    single<OkHttpClient>(named(GLIDE_OK_HTTP))  {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        OkHttpClient.Builder().addInterceptor(interceptor).connectTimeout(5, TimeUnit.MINUTES)
            .writeTimeout(5, TimeUnit.MINUTES)
            .readTimeout(10, TimeUnit.MINUTES).build()
    }

    single<OkHttpClient> {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        OkHttpClient.Builder().addInterceptor(interceptor).connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS).build()
    }

    single<Retrofit> {
        val builder = Moshi.Builder()

        builder.add(RealmListJsonAdapterFactory())
            .add(Date::class.java, Rfc3339DateJsonAdapter())
            .add(KotlinJsonAdapterFactory())

        Retrofit.Builder()
            .baseUrl(get<Architecture.RestConfiguration>().url)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(
                MoshiConverterFactory.create(builder.build())
            )
            .client(get())
            .build()
    }


    single<Architecture.RestConfiguration> {
        FutureMindRestConfiguration()
    }
}
