package com.example.future_mind.base.binding

import android.annotation.SuppressLint
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.future_mind.R
import com.example.future_mind.base.Architecture
import com.example.future_mind.base.utils.GlideApp
import com.google.android.material.progressindicator.LinearProgressIndicator
import org.koin.java.KoinJavaComponent.inject
import java.util.*


object BindingAdapters {
    @BindingAdapter("adapter")
    @JvmStatic
    fun <T : RecyclerView.ViewHolder?> bindAdapterForRecyclerView(
        view: RecyclerView,
        adapter: RecyclerView.Adapter<T>?
    ) {
        adapter?.let { view.adapter = it }

    }

    @BindingAdapter("imageUrl")
    @JvmStatic
    fun loadImage(view: ImageView, url: String?) {
        if (!url.isNullOrEmpty()) {
            var requestOptions = RequestOptions()
            requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(16))
            GlideApp.with(view.context)
                .load(url)
                .apply(requestOptions)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.ic_launcher_background)

                .into(view)
        }
    }

    @BindingAdapter("date")
    @JvmStatic
    fun setDate(view: TextView, date: Date?) {
        val formatter: Architecture.DateFormatter by inject(Architecture.DateFormatter::class.java)
        view.text = formatter.formatDate(date)
    }

    @BindingAdapter("uiState")
    @JvmStatic
    fun isShowProgress(view: LinearProgressIndicator, uiState: Architecture.UiState?) {
        if (uiState == Architecture.UiState.LOADING) {
            view.visibility = View.VISIBLE
        } else {
            view.visibility = View.GONE

        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    @BindingAdapter("loadUrl")
    @JvmStatic
    fun loadUrl(view: WebView, url: String?) {
        if (!url.isNullOrEmpty()) {
            view.webViewClient = WebViewClient()
            view.webChromeClient = WebChromeClient()
            view.settings.javaScriptEnabled = true
            view.settings.loadWithOverviewMode = true
            view.settings.useWideViewPort = true
            view.loadUrl(url)
        }

    }
}