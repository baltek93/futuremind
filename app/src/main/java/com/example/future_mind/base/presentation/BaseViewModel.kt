package com.example.future_mind.base.presentation

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModel
import com.example.future_mind.base.Architecture
import com.example.future_mind.base.utils.SingleLiveEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BaseViewModel : ViewModel() {

    private val mDisposables = CompositeDisposable()
    val navigationCommands = SingleLiveEvent<Architecture.NavigationCommand>()

    protected fun Disposable.track() {
        mDisposables.add(this)
    }

    override fun onCleared() {
        mDisposables.clear()
        super.onCleared()
    }
    open fun prepare(args: Bundle?)
    {
        Log.w("App", "prepare: ${this.javaClass.simpleName}")
    }

    fun navigateTo(directions: Architecture.NavigationCommand)
    {
        navigationCommands.postValue(directions)
    }
}