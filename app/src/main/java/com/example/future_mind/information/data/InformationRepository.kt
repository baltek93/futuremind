package com.example.future_mind.information.data

import com.example.future_mind.base.Architecture
import com.example.future_mind.base.data.local.Predicate
import com.example.future_mind.base.utils.SeparatorDescriptionAndUrlImpl
import com.example.future_mind.information.InformationContract
import com.example.future_mind.information.data.model.InformationModel
import com.example.future_mind.information.data.remote.RemoteInformationRepository
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class InformationRepository(
    private val localRepository: Architecture.LocalRepository,
    private val remoteInformationRepository: RemoteInformationRepository,
    private val separateDescription: Architecture.SeparatorDescriptionAndUrl
) : InformationContract.InformationRepository {
    override fun fetchInformation(): Completable {
        return remoteInformationRepository.getRemoteInformation().subscribeOn(Schedulers.io())
            .map { informationList ->
                informationList.map {
                    it.onlyDescription = separateDescription.getOnlyDescription(it.description)
                    it.url = separateDescription.getOnlyUrl(it.description)
                }
                informationList
            }.observeOn(AndroidSchedulers.mainThread()).flatMapCompletable {
            Completable.fromCallable {
                localRepository.saveData(it)
            }
        }
    }

    override fun getInformation(predicate: Predicate?): Observable<List<InformationModel>> {
        return localRepository.getListObservable(InformationModel::class.java, predicate)
    }

}