package com.example.future_mind.information.ui.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.future_mind.information.data.model.InformationModel
import com.example.future_mind.databinding.InformationItemBinding

class InformationAdapter(
    private var items: List<InformationModel>,
    private val onClickItem: ((url: String) -> Unit?)
) : RecyclerView.Adapter<InformationAdapter.ViewHolder>() {

    fun setData(items: List<InformationModel>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = InformationItemBinding.inflate(inflater)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])


    inner class ViewHolder(private val binding: InformationItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: InformationModel) {
            binding.root.setOnClickListener {
                onClickItem.invoke(item.url)
            }
            binding.item = item
            binding.executePendingBindings()
        }
    }
}