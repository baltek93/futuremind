package com.example.future_mind.information.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.future_mind.base.presentation.BaseFragment
import com.example.future_mind.R
import com.example.future_mind.databinding.InformationDetailFragmentBinding
import org.koin.android.viewmodel.ext.android.viewModel

class InformationDetailsFragment :
    BaseFragment<InformationDetailsViewModel>(R.layout.information_detail_fragment) {

    override val viewModel: InformationDetailsViewModel by viewModel()
    private var _binding: InformationDetailFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.getUrl().observe(viewLifecycleOwner) {
            binding.url = it
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = InformationDetailFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

}