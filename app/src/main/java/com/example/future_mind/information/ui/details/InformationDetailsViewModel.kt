package com.example.future_mind.information.ui.details

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.future_mind.base.presentation.BaseViewModel

class InformationDetailsViewModel : BaseViewModel() {

    private var url = MutableLiveData<String>()

    fun getUrl(): LiveData<String> = url

    override fun prepare(args: Bundle?) {
        super.prepare(args)
        args?.let {
            val navigationParam = InformationDetailsFragmentArgs.fromBundle(it).url
            url.postValue(navigationParam)
        }
    }
}