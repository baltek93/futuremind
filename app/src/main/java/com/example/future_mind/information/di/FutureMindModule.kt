package com.example.future_mind.information.di

import com.example.future_mind.base.Architecture
import com.example.future_mind.base.utils.DateFormatterImpl
import com.example.future_mind.base.utils.SeparatorDescriptionAndUrlImpl
import com.example.future_mind.information.data.InformationRepository
import com.example.future_mind.information.data.remote.RemoteInformationRepository
import com.example.future_mind.information.InformationContract
import com.example.future_mind.information.ui.details.InformationDetailsViewModel
import com.example.future_mind.information.ui.list.InformationListViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val futureMindModule = module {
    single<RemoteInformationRepository> { get<Retrofit>().create(RemoteInformationRepository::class.java) }

    //repositories
    factory<InformationContract.InformationRepository> {
        InformationRepository(get(), get(), get())
    }

    //viewModels
    viewModel { InformationListViewModel(get(),get()) }
    viewModel { InformationDetailsViewModel() }

    //Utils
    factory<Architecture.SeparatorDescriptionAndUrl> {
        SeparatorDescriptionAndUrlImpl()
    }

    factory<Architecture.DateFormatter> {
        DateFormatterImpl()
    }


}