package com.example.future_mind.information

import com.example.future_mind.base.data.local.Predicate
import com.example.future_mind.information.data.model.InformationModel
import io.reactivex.Completable
import io.reactivex.Observable

interface InformationContract {

    interface InformationRepository {
        fun fetchInformation(): Completable
        fun getInformation(predicate: Predicate?=null): Observable<List<InformationModel>>
    }

}