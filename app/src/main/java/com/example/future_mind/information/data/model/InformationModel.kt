package com.example.future_mind.information.data.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class InformationModel(
    @PrimaryKey
    var orderId: Long = 0,
    var description: String = "",
    var image_url: String = "",
    var modificationDate: Date? = null,
    var title: String = "",
    var onlyDescription: String = "",
    var url: String = "",
) : RealmObject()
{
    companion object{
     const val FIELD_ORDER_ID= "orderId"
    }
}