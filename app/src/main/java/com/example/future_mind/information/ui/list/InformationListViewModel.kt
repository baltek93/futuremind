package com.example.future_mind.information.ui.list

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.future_mind.base.Architecture
import com.example.future_mind.base.data.local.Order
import com.example.future_mind.base.data.local.PredicateFactory
import com.example.future_mind.base.presentation.BaseViewModel
import com.example.future_mind.information.InformationContract
import com.example.future_mind.information.data.model.InformationModel
import com.example.future_mind.information.data.model.InformationModel.Companion.FIELD_ORDER_ID
import com.example.future_mind.information.ui.details.InformationDetailsFragmentDirections
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy

class InformationListViewModel(
    private val repository: InformationContract.InformationRepository,
    predicateFactory: PredicateFactory
) :
    BaseViewModel() {

    private var _tempLocalDispossable: Disposable? = null
    private var _tempRemoteDispossable: Disposable? = null

    private val _listInformation = MutableLiveData<List<InformationModel>>()
    private val _uiState = MutableLiveData<Architecture.UiState>()
    private val _sortByOrder = predicateFactory.sort(null, FIELD_ORDER_ID, Order.ASCENDING)

    val uiState: LiveData<Architecture.UiState> = _uiState
    val listInformation: LiveData<List<InformationModel>> = _listInformation

    init {
        refreshRemoteDataInformation()
        getLocalDataInformation()
    }

    private fun getLocalDataInformation() {
        if (_tempLocalDispossable?.isDisposed != true) {
            _tempLocalDispossable?.dispose()
        }
        _tempLocalDispossable =
            repository.getInformation(_sortByOrder).subscribeBy(onNext = { informationList ->
                _listInformation.postValue(informationList)
                _tempLocalDispossable?.track()

            },
                onError = {
                })
    }


    fun refreshRemoteDataInformation() {
        _uiState.postValue(Architecture.UiState.LOADING)
        if (_tempRemoteDispossable?.isDisposed != true) {
            _tempRemoteDispossable?.dispose()
        }
        _tempRemoteDispossable = repository.fetchInformation().subscribeBy(onComplete = {
            _tempRemoteDispossable?.track()
            _uiState.postValue(Architecture.UiState.SUCCESS)
            Log.e("COMPLETABLE SUCCESS ", "COMPLETABLE")
        }, onError = {
            _uiState.postValue(Architecture.UiState.FAILED)

        })
    }

    fun navigateToDetails(url: String, isTablet: Boolean) {
        if (isTablet) {
            navigateTo(
                Architecture.NavigationCommand.ToOtherNavigation(
                    InformationDetailsFragmentDirections.actionListToDetails(url)
                )
            )
        } else {
            navigateTo(
                Architecture.NavigationCommand.To(
                    InformationListFragmentDirections.actionListToDetails(url)
                )
            )
        }

    }
}