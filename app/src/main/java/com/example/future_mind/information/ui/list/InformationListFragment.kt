package com.example.future_mind.information.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.future_mind.base.presentation.BaseFragment
import com.example.future_mind.R
import com.example.future_mind.base.Architecture
import com.example.future_mind.databinding.InformationFragmentBinding
import com.google.android.material.snackbar.Snackbar
import org.koin.android.viewmodel.ext.android.viewModel

class InformationListFragment : BaseFragment<InformationListViewModel>(
    R.layout.information_fragment
) {

    override val viewModel: InformationListViewModel by viewModel()
    private var _binding: InformationFragmentBinding? = null
    private val binding get() = _binding!!
    private val _informationAdapter = InformationAdapter(emptyList()) {
        viewModel.navigateToDetails(it, resources.getBoolean(R.bool.isTablet))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = InformationFragmentBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.adapter = _informationAdapter

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.listInformation.observe(viewLifecycleOwner)
        {
            _informationAdapter.setData(it)
        }
        viewModel.uiState.observe(viewLifecycleOwner) {
            showErrorNetwork(it)
            binding.viewModel = viewModel
        }
    }

    private fun showErrorNetwork(it: Architecture.UiState?) {
        if (isShowNetworkError(it))
            Snackbar.make(
                binding.root,
                getString(R.string.core_network_error),
                Snackbar.LENGTH_LONG
            ).show()
    }

    private fun isShowNetworkError(it: Architecture.UiState?) =
        it == Architecture.UiState.FAILED && _informationAdapter.itemCount == 0

    override fun onDestroyView() {
        super.onDestroyView()
        binding.adapter = null
        _binding = null

    }


}