package com.example.future_mind.information.data.remote

import com.example.future_mind.information.data.model.InformationModel
import io.reactivex.Single
import retrofit2.http.GET

interface RemoteInformationRepository {
    @GET("recruitment-task")
    fun getRemoteInformation(): Single<List<InformationModel>>

}