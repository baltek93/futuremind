package com.example.future_mind.information.data.remote

import com.example.future_mind.base.Architecture

class FutureMindRestConfiguration(override val url: String = "https://recruitment-task.futuremind.dev/") :
    Architecture.RestConfiguration