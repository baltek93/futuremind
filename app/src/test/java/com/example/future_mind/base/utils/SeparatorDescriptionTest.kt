package com.example.future_mind.base.utils

import org.junit.Test

class SeparatorDescriptionTest {

    private val separatorDescription by lazy { SeparatorDescriptionAndUrlImpl() }
    private val onlyDescription = "description "
    private val onlyUrl = "https://www.onet.pl/"
    private val descriptionWithUrl = "$onlyDescription\t$onlyUrl"

    @Test
    fun simple_separate_description_should_work() {
        assert(separatorDescription.getOnlyDescription(descriptionWithUrl) == onlyDescription)
    }

    @Test
    fun simple_separate_url_should_work() {
        assert(separatorDescription.getOnlyUrl(descriptionWithUrl) == onlyUrl)
    }
}